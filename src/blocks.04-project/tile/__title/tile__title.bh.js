module.exports = function(bh) {
  bh.match('tile__title', function(ctx, json) {
    ctx.tag('h4');
    ctx.attr('id', ctx.generateId());
  });
};
