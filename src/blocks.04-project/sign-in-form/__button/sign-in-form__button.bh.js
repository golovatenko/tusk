module.exports = function(bh) {
  bh.match('sign-in-form__button', function(ctx, json) {
    ctx.attr('id', ctx.generateId());
    json.placeholder && ctx.attr('placeholder', json.placeholder);
    ctx.attr('value', ctx.content());
    ctx.tag('input').content(false, true);
    ctx.attr('type', 'submit');
    if (ctx.attr('type') == 'password') {
      ctx.attr('autocomplete', 'current-password');
    }
  });
};
