module.exports = {
  block: 'page',
  title: 'first-tusk',
  content: [
    require('./common/header.bemjson'),
      {block: 'products-gallery', cls: 'row', content: [
        'Some quick example text to build on the card title and make up the bulk of the card content 1',
          'Some quick example text to build on the card title and make up the bulk of the card content 2' +
          'Some quick example text to build on the card title and make up the bulk of the card content 2' +
          'Some quick example text to build on the card title and make up the bulk of the card content 2',
          'Some quick example text 3',
          'Some quick example text to 4',
          'Some quick 5',
          'Some quick example text to build on the card title and make up the bulk of the card content 6' +
          'Some quick example text to build on the card title and make up the bulk of the card content 6' +
          'Some quick example text to build on the card title and make up the bulk of the card content 6',
        ].map((text) => [
          {block: 'product-card', cls: 'col-sm-6 col-md-4 col-lg-4', content: [
              {elem: 'body', cls: 'card-body', content: [
                  {elem: 'image', content: [
                      {block: 'img', mods: {lazy: true}, src: 'http://placehold.it/150x150', content: 'my image'},
                    ]},
                  {elem: 'title', cls: ' h3 card-title', content: 'card-title'},
                  {elem: 'text', cls: 'card-text', content: text},
                  {elem: 'button', cls: 'btn btn-primary', content: 'Go somewhere'},
                ],
              }],
          }])},
    /* ==============================================================================================================================*/
    {block: 'products-gallery2', content: [
      {block: 'product-card2', content: [
        {elem: 'body', content: [
            {elem: 'image', content: [
              {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
              ]},
            {elem: 'title', content: '1'},
            {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
            {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
          ]}],
      },
        {block: 'for-float'},
        {block: 'product-card2', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x500', content: 'my image'},
                  ]},
                {elem: 'title', content: '2'},
                {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
                {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
              ]}],
        },
        {block: 'for-float'},
        {block: 'product-card2', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/100x100', content: 'my image'},
                  ]},
                {elem: 'title', content: '3'},
                {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
                {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
              ]}],
        },
        {block: 'for-float'},
        {block: 'product-card2', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '4'},
                {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
                {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
              ]}],
        },
        {block: 'for-float'},
        {block: 'product-card2', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '5'},
                {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
                {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
              ]}],
        },
        {block: 'for-float'},
        {block: 'product-card2', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '6'},
                {elem: 'text', content: 'Some quick example text to build on the card title and make up the bulk of the card'},
                {elem: 'button', attrs: {type: 'submit'}, content: 'Submit'},
              ]}],
        },
        /* ==============================================================================================================================*/
        {block: 'for-float'},
      ]},
    {block: 'product-gallery3', content: [
      {block: 'product-card3', content: [
          {elem: 'body', content: [
              {elem: 'image', content: [
                  {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                ]},
              {elem: 'title', content: '1'},
              {elem: 'text', content: 'Some quick example text to build'},
              {elem: 'button', content: 'Submit'},
            ]},
        ]},
        {block: 'product-card3', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x500', content: 'my image'},
                  ]},
                {elem: 'title', content: '2'},
                {elem: 'text', content: 'Some quick example text to build Some quick example text to build Some quick example text to build' +
                    'Some quick example text to build Some quick example text to build Some quick example text to build'},
                {elem: 'button', content: 'Submit'},
              ]},
          ]},
        {block: 'product-card3', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '3'},
                {elem: 'text', content: 'Some quick example text'},
                {elem: 'button', content: 'Submit'},
              ]},
          ]},
        {block: 'product-card3', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '4'},
                {elem: 'text', content: 'Some quick example text to build'},
                {elem: 'button', content: 'Submit'},
              ]},
          ]},
        {block: 'product-card3', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '5'},
                {elem: 'text', content: 'Some quick example text to build'},
                {elem: 'button', content: 'Submit'},
              ]},
          ]},
        {block: 'product-card3', content: [
            {elem: 'body', content: [
                {elem: 'image', content: [
                    {block: 'img', src: 'http://placehold.it/300x200', content: 'my image'},
                  ]},
                {elem: 'title', content: '6'},
                {elem: 'text', content: 'Some quick example text to build'},
                {elem: 'button', content: 'Submit'},
              ]},
          ]},
    ]},
    /* ==============================================================================================================================*/
    {block: 'centering-tusk', content: [
      {block: 'sign-in-form', content: [
        {elem: 'h2', content: 'Авторизация'},
          {block: 'user-name', content: [
              {elem: 'label', tag: 'label', content: 'Email', attrs: {'for': 'uniq16348376738551878'}},
              {elem: 'br', tag: 'br'},
              {block: 'input'},
        ]},
          {block: 'user-password', content: [
              {elem: 'label', tag: 'label', content: 'Password', attrs: {'for': 'uniq16348376738551878'}},
              {elem: 'br', tag: 'br'},
              {block: 'input', attrs: {type: 'password'}},
            ]},
          {elem: 'button', content: 'Log in'},
        ]},
      ]},
    {block: 'new-block-for-centering', content: 'null'},
    require('./common/footer.bemjson'),
  ],
};
