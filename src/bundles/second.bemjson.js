module.exports = {
  block: 'page',
  title: 'Пустая',
  content: [
    require('./common/header.bemjson'),
    {block: 'tile', content: [
        {elem: 'title', tag: 'h2', content: 'title'},
      ]},
    require('./common/footer.bemjson'),
  ],
};
